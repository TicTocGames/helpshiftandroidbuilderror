﻿using UnityEngine;
using UnityEngine.UI;

namespace TicToc.Dolittle.Shared.HelpshiftPlugin
{
	public class HelpshiftButtonShowFaqs : MonoBehaviour
	{
		[SerializeField] private Button _button;

		private void Reset()
		{
			_button = GetComponent<Button>();
		}

		private void OnEnable()
		{
			_button.onClick.AddListener(HandleButtonClick);
		}

		private void OnDisable()
		{
			if (_button)
				_button.onClick.RemoveListener(HandleButtonClick);
		}

		private void HandleButtonClick()
		{
			HelpshiftService.ShowFaqs();
		}
	}
}

﻿using System;
using Helpshift;
using UnityEngine;

namespace TicToc.Dolittle.Shared.HelpshiftPlugin
{
    public static class HelpshiftService
    {
	    private static HelpshiftSdk Instance
	    {
		    get
		    {
			    // the sdk will return null if not ios or android, but since an editor can still pass those flags, this wraps ensuring we don't get errors in editor mode
#if UNITY_EDITOR || UNITY_STANDALONE
			    return null;
#endif
			    return HelpshiftSdk.getInstance();
		    }
	    }
	    
	    [RuntimeInitializeOnLoadMethod]
	    private static void Install()
	    {
			// install using config values (Change in Unity Helpshift menu bar)
			var apiKey = "de4ca69ac99ba6ae29e5901a9428ea18";
			var domain = "nbcuniversal.helpshift.com";
			var androidAppId = "nbcuniversal_platform_20190327190001298-88b47868087b26b";
			Instance?.install(apiKey, domain, androidAppId);
	    }

	    /// <summary>
	    /// Triggers the helpshift show FAQs if on an appropriate platform 
	    /// </summary>
	    public static void ShowFaqs()
        {
	        Instance?.showFAQs();
        }
    }
}
